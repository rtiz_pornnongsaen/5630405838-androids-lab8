package th.ac.kku.pornnongsaen.ahrthit.communicationwithintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
    }
    @Override
    public void finish(){
        Intent data = new Intent();
        data.putExtra("returnKey1","Swinging on a star.");
        data.putExtra("returnKey2","You could be better then you are");
        setResult(RESULT_OK, data);
        super.finish();
    }
}
