package th.ac.kku.pornnongsaen.ahrthit.communicationwithintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    int REQUEST_CODE = 10;
    Button ok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //ok = (Button) findViewById(R.id.button);

        //ok.setOnClickListener();
    }


       /* // constant to determine which sub-activity returns private static final int REQUEST_CODE = 10;
    public void okButtonClicked(View view) {
        EditText name = (EditText) findViewById (R.id.editText);
        // To do 1. create new intent
        Intent i = new Intent(this,ResultActivity.class);
        i.putExtra("Value1","This value one for ActivityTwo");
        i.putExtra("Value2","This value two ActivityTwo");
        // To do 2. use startActivityForResult
        startActivityForResult(i, 10);
        //  with REQUEST_CODE

    }*/
         public void onClick(View view) {

            Intent i = new Intent(this, ResultActivity.class);

            i.putExtra("Value1", "This value one for ActivityTwo ");

            i.putExtra("Value2", "This value two ActivityTwo");

            startActivityForResult(i, 10);
        }

    @Override protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
            if(data.hasExtra("returnKey1")){
                Toast.makeText(this,data.getExtras().getString("returnKey1"),Toast.LENGTH_SHORT).show();            }
            // Todo 1. get data from ResultActivity
            // Todo 2. set the value of TextView with
            // the received data
        }
    }
}
